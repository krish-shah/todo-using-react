import React, { useRef, useState } from 'react';
import { Container } from '@mui/material';
import { Header } from './components/Header';
import { TaskForm } from './components/TaskForm';
import { TaskDashboard } from './components/TaskDashboard';
import {v4 as uuidv4} from 'uuid';

const App = () => {

    const [tasks, setTasks] = useState([]);
    const taskRef = useRef();
    const [priority, setPriority] = useState('Low');

    function clearTask() {
        taskRef.current.value = '';
    }

    function handleToggleCompletion(taskId) {
        // const updatedTasks = tasks.map(task => {
        //     if(task.id === taskId) {
        //         return {
        //             ...task,
        //             isCompleted: !task.isCompleted
        //         };
        //     }
        //     return task;
        // });
        // setTasks(updatedTasks);
        setTasks(tasks.map(task => task.id === taskId ? { ...task, isCompleted: !task.isCompleted } : task));
    }

    function handleDelete(taskId) {
        setTasks(tasks.filter(task => task.id !== taskId));
    }

    function submitTask(evt) {
        evt.preventDefault();
        if(taskRef.current.value === '') 
            alert("Blank output is not allowed");
        else {
            setTasks(tasks.concat({id: uuidv4(), value: taskRef.current.value, isCompleted:false, priority:priority}));
            taskRef.current.value = '';
        }
    }
    return (
        <div>
            <Header />     
            <Container maxWidth='lg' sx={{ marginTop: 4 }}>  
                <TaskForm taskRef={taskRef} onClear={clearTask} onSubmit={submitTask} priority={priority} changePriority={setPriority}/>           
                <TaskDashboard tasks={tasks} onToggle={handleToggleCompletion} onDelete={handleDelete}/>
            </Container>
        </div>
    );
};

export default App;
