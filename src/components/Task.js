import { Button, Checkbox, Chip, ListItem, ListItemSecondaryAction, ListItemText } from '@mui/material'
import DeleteForeverOutlinedIcon from '@mui/icons-material/DeleteForeverOutlined';
import React from 'react'

export const Task = ({task, onToggle, onDelete}) => {
    const textDecorationStyle = task.isCompleted ? { textDecoration: "line-through" } : {};
    let chipElement;

    if(task.isCompleted) {
        chipElement = <Chip label='Completed' variant='outlined' color="success" />;
    } else {
        chipElement = <Chip label='Pending' variant='outlined' color="warning" />;
    }

    let priorityChipElement;
    if(task.priority === 'Low')
        priorityChipElement = <Chip sx={{marginRight: 5}} label='Low' variant='fill' color='success' />;
    else if(task.priority === 'Medium')
        priorityChipElement = <Chip sx={{marginRight: 5}} label='Medium' variant='fill' color='warning' />;
    else 
        priorityChipElement = <Chip sx={{marginRight: 5}} label='High' variant='fill' color='error' />;

  return (
    <ListItem sx={{ paddingBottom: 2, paddingTop: 2, borderBottom: 'solid 1px #AAA' }} disablePadding>
        <Checkbox 
            checked={task.isCompleted} 
            onChange={ () => onToggle(task.id) }
        />
        <ListItemText 
            primary={ task.value } 
            style={textDecorationStyle}
        />
        
        <ListItemSecondaryAction>    
            { priorityChipElement }
            { chipElement }
            <Button 
                variant="contained" 
                color="error" 
                sx={{marginLeft: 5 }}
                onClick={() => onDelete(task.id)}
            >
                <DeleteForeverOutlinedIcon/> Delete
            </Button>
        </ListItemSecondaryAction>
    </ListItem>
  )
}
