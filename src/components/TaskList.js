import { List } from '@mui/material'
import React from 'react'
import { Task } from './Task'

export const TaskList = ({tasks, onToggle, onDelete, order, searchKey}) => {
  function ascendingSort(tasks) {
    const priorityOrder = {'Low': 1, 'Medium': 2, 'High': 3};
    tasks.sort((taskA, taskB) => {
      const priorityA = priorityOrder[taskA.priority];
      const priorityB = priorityOrder[taskB.priority];
  
      return priorityA - priorityB;
    });
  }
  
  function descendingSort(tasks) {
    const priorityOrder = {'Low': 3, 'Medium': 2, 'High': 1};
    tasks.sort((taskA, taskB) => {
      const priorityA = priorityOrder[taskA.priority];
      const priorityB = priorityOrder[taskB.priority];
  
      return priorityA - priorityB;
    });
  }

  function searchTasks() {
    return tasks.filter(task=>task.value.toLowerCase().includes(searchKey.toLowerCase()));
  }

  return (
    <List sx={{ padding: 1}}>
        {order === 'Descending' ? descendingSort(tasks) : ascendingSort(tasks)}
        {searchKey !== '' ? searchTasks().map(task=> <Task key={task.id} task={task} onToggle={onToggle} onDelete={onDelete}/>) : tasks.map(task=> <Task key={task.id} task={task} onToggle={onToggle} onDelete={onDelete}/>)}
        {/* {tasks.map(task=> <Task key={task.id} task={task} onToggle={onToggle} onDelete={onDelete}/>)} */}
    </List>
  )
}
