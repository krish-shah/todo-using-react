import {Card, FormControl, Grid, InputLabel, MenuItem, Select, TextField } from '@mui/material'
import React, { useState } from 'react'
import { TaskList } from './TaskList'

export const TaskDashboard = ({tasks, onToggle, onDelete}) => {
  const [order, setOrder] = useState('Descending');
  const [search, setSearch] = useState('');

  function handlePriorityChange(evt) {
      setOrder(evt.target.value);
  }

  function handleSearchChange(evt) {
    setSearch(evt.target.value);
  }
  return (
    <Grid container justifyContent="center" sx={{ marginTop: 4 }}>
        <Grid item xs={12}>
            <Card sx={{ padding: 1 }} raised={true}>
                <Grid container spacing={1} alignItems="center">
                    <Grid item xs={3}>
                        <TextField 
                          id="outlined-basic" 
                          label="Search" 
                          variant="outlined" 
                          onChange={handleSearchChange}
                        />
                    </Grid>
                    <Grid item xs={3}>
                        <FormControl fullWidth>
                            <InputLabel id="demo-simple-select-label">Priority</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={order}
                                    label="Priority"
                                    onChange={handlePriorityChange}
                                >
                                    <MenuItem value={'Ascending'}>Low to High</MenuItem>
                                    <MenuItem value={'Descending'}>High to Low</MenuItem>
                                </Select>
                        </FormControl>
                    </Grid>
                </Grid>
                <TaskList tasks={tasks} onToggle={onToggle} onDelete={onDelete} order={order} searchKey={search}/>
            </Card>
        </Grid>
    </Grid>
  )
}
