import { Button, Card, FormControl, Grid, InputLabel, MenuItem, Select, TextField } from '@mui/material'
import DataSaverOnOutlinedIcon from '@mui/icons-material/DataSaverOnOutlined';
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined';
import React from 'react'

export const TaskForm = ({taskRef, onClear, onSubmit, priority, changePriority}) => {
    function handlePriorityChange(evt) {
        changePriority(evt.target.value);
    }
  return (
    <Grid container justifyContent="center" >
        <Grid item xs={12}>
            <Card sx={{ padding: 4 }} raised={true}>
                <Grid container spacing={2} alignItems="center">
                    <Grid item xs={6}>
                        <form onSubmit={onSubmit}>
                            <TextField 
                                inputRef={taskRef}
                                id="task-input" 
                                label="Insert your task" 
                                fullWidth
                            />
                        </form>
                    </Grid>
                    <Grid item xs={2}>
                    <FormControl fullWidth>
                        <InputLabel id="demo-simple-select-label">Priority</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={priority}
                            label="Priority"
                            onChange={handlePriorityChange}
                        >
                            <MenuItem value={'Low'}>Low</MenuItem>
                            <MenuItem value={'Medium'}>Medium</MenuItem>
                            <MenuItem value={'High'}>High</MenuItem>
                        </Select>
                    </FormControl>    
                    </Grid>
                    <Grid item xs={2}>
                        <Button 
                            variant="contained" 
                            color="primary" 
                            fullWidth
                            onClick={onSubmit}
                        >
                            <DataSaverOnOutlinedIcon/> Submit
                        </Button>
                    </Grid>
                    <Grid item xs={2}>
                        <Button 
                            variant="contained" 
                            color="secondary" 
                            fullWidth
                            onClick={onClear}
                        >
                            <DeleteOutlinedIcon/> Clear
                        </Button>
                    </Grid>
                </Grid>
            </Card>
        </Grid>
    </Grid>
  )
}
